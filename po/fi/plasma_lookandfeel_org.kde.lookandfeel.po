# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2014, 2015, 2016, 2017, 2019.
# Tommi Nieminen <translator@legisign.org>, 2016, 2017, 2018, 2019, 2020.
#
#
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-01-16 02:49+0100\n"
"PO-Revision-Date: 2020-10-23 23:22+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"

#: ../sddm-theme/KeyboardButton.qml:13
msgid "Keyboard Layout: %1"
msgstr "Näppäimistöasettelu: %1"

#: ../sddm-theme/Login.qml:59
msgid "Username"
msgstr "Käyttäjätunnus"

#: ../sddm-theme/Login.qml:75 contents/lockscreen/MainBlock.qml:62
msgid "Password"
msgstr "Salasana"

#: ../sddm-theme/Login.qml:114
msgid "Log In"
msgstr "Kirjaudu"

#: ../sddm-theme/Main.qml:198 contents/lockscreen/LockScreenUi.qml:251
msgid "Caps Lock is on"
msgstr "Vaihtolukko on käytössä"

#: ../sddm-theme/Main.qml:210 ../sddm-theme/Main.qml:398
#: contents/logout/Logout.qml:176
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Valmiustila"

#: ../sddm-theme/Main.qml:218 ../sddm-theme/Main.qml:406
#: contents/logout/Logout.qml:194
msgid "Restart"
msgstr "Käynnistä uudelleen"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:414
#: contents/logout/Logout.qml:204
msgid "Shut Down"
msgstr "Sammuta"

#: ../sddm-theme/Main.qml:234
msgctxt "For switching to a username and password prompt"
msgid "Other..."
msgstr "Muu…"

#: ../sddm-theme/Main.qml:386
msgid "Type in Username and Password"
msgstr "Syötä käyttäjätunnus ja salasana"

#: ../sddm-theme/Main.qml:422
msgid "List Users"
msgstr "Luettele käyttäjät"

#: ../sddm-theme/Main.qml:490 contents/lockscreen/LockScreenUi.qml:513
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Virtuaalinäppäimistö"

#: ../sddm-theme/Main.qml:519
msgid "Login Failed"
msgstr "Kirjautuminen epäonnistui"

#: ../sddm-theme/SessionButton.qml:35
msgid "Desktop Session: %1"
msgstr "Työpöytäistunto: %1"

#: contents/components/Battery.qml:53
msgid "%1%"
msgstr "%1 %"

#: contents/components/Battery.qml:54
msgid "Battery at %1%"
msgstr "Akun varaus %1 %"

#: contents/components/UserList.qml:62
msgctxt "Nobody logged in on that session"
msgid "Unused"
msgstr "Käyttämätön"

#: contents/components/UserList.qml:68
msgctxt "User logged in on console number"
msgid "TTY %1"
msgstr "TTY %1"

#: contents/components/UserList.qml:70
msgctxt "User logged in on console (X display number)"
msgid "on TTY %1 (Display %2)"
msgstr "TTY:ssä %1 (näytöllä %2)"

#: contents/components/UserList.qml:74
msgctxt "Username (location)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/lockscreen/config.qml:17
msgctxt "verb, to show something"
msgid "Show:"
msgstr "Näytä:"

#: contents/lockscreen/config.qml:18
msgid "Clock"
msgstr "Kello"

#: contents/lockscreen/config.qml:26
msgid "Media Controls"
msgstr "Mediasäätimet"

#: contents/lockscreen/LockScreenUi.qml:45
msgid "Unlocking failed"
msgstr "Lukituksen avaaminen epäonnistui"

#: contents/lockscreen/LockScreenUi.qml:267
msgid "Switch User"
msgstr "Vaihda käyttäjää"

#: contents/lockscreen/LockScreenUi.qml:461
msgid "Switch to This Session"
msgstr "Vaihda tähän istuntoon"

#: contents/lockscreen/LockScreenUi.qml:469
msgid "Start New Session"
msgstr "Käynnistä uusi istunto"

#: contents/lockscreen/LockScreenUi.qml:482
msgid "Back"
msgstr "Takaisin"

#: contents/lockscreen/MainBlock.qml:105
msgid "Unlock"
msgstr "Avaa"

#: contents/lockscreen/MediaControls.qml:118
msgid "No media playing"
msgstr "Mitään ei toisteta"

#: contents/lockscreen/MediaControls.qml:146
msgid "Previous track"
msgstr "Edellinen kappale"

#: contents/lockscreen/MediaControls.qml:157
msgid "Play or Pause media"
msgstr "Toista tai keskeytä media"

#: contents/lockscreen/MediaControls.qml:170
msgid "Next track"
msgstr "Seuraava kappale"

#: contents/logout/Logout.qml:152
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Kirjautuneena on toinenkin käyttäjä. Jos tietokone sammutetaan tai "
"käynnistetään uudelleen, hän voi menettää työnsä."
msgstr[1] ""
"Kirjautuneena on %1 muutakin käyttäjää. Jos tietokone sammutetaan tai "
"käynnistetään uudelleen, he voivat menettää työnsä."

#: contents/logout/Logout.qml:166
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr ""
"Uudelleenkäynnistettäessä tietokone siirtyy laiteohjelmiston asetusnäyttöön."

#: contents/logout/Logout.qml:185
msgid "Hibernate"
msgstr "Lepotila"

#: contents/logout/Logout.qml:214
msgid "Log Out"
msgstr "Kirjaudu ulos"

#: contents/logout/Logout.qml:237
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Käynnistetään uudelleen sekunnin kuluttua"
msgstr[1] "Käynnistetään uudelleen %1 sekunnissa"

#: contents/logout/Logout.qml:239
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Sammutus 1 sekunnin kuluttua"
msgstr[1] "Sammutus %1 sekunnin kuluttua"

#: contents/logout/Logout.qml:241
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Uloskirjautuminen 1 sekunnin kuluttua"
msgstr[1] "Uloskirjautuminen %1 sekunnin kuluttua"

#: contents/logout/Logout.qml:252
msgid "OK"
msgstr "OK"

#: contents/logout/Logout.qml:258
msgid "Cancel"
msgstr "Peru"

#: contents/osd/OsdItem.qml:71
msgid "100%"
msgstr "100 %"

#: contents/osd/OsdItem.qml:85
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1 %"

#: contents/runcommand/RunCommand.qml:85
msgid "Configure"
msgstr "Asetukset"

#: contents/runcommand/RunCommand.qml:86
msgid "Configure Search Plugins"
msgstr "Hakuliitännäisten asetukset"

#: contents/runcommand/RunCommand.qml:89
msgid "Configure KRunner..."
msgstr "KRunnerin asetukset…"

#: contents/runcommand/RunCommand.qml:103
msgctxt "Textfield placeholder text, query specific KRunner"
msgid "Search '%1'..."
msgstr "Etsi ”%1”…"

#: contents/runcommand/RunCommand.qml:105
msgctxt "Textfield placeholder text"
msgid "Search..."
msgstr "Etsi…"

# *** TARKISTA: Voisi viitata myös johonkin PIN-koodiin tms.!
#: contents/runcommand/RunCommand.qml:227
msgid "Pin"
msgstr "Kiinnitä"

# *** TARKISTA: Toinen mahdollisuus voisi olla jokin ”PIN-haku” tms.!
#: contents/runcommand/RunCommand.qml:228
msgid "Pin Search"
msgstr "Kiinnitä haku"

#: contents/runcommand/RunCommand.qml:230
msgid "Keep Open"
msgstr "Pidä avoinna"

#: contents/runcommand/RunCommand.qml:291
msgid "Recent Queries"
msgstr "Viimeisimmät haut"

#: contents/runcommand/RunCommand.qml:294
msgid "Remove"
msgstr "Poista"

#: contents/runcommand/RunCommand.qml:296
msgid "in category recent queries"
msgstr "viimeisimpien hakujen luokassa"

#: contents/splash/Splash.qml:95
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma KDE:ltä"