# Translation of plasma_package_plasmashell.po to Brazilian Portuguese
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: plasma_package_plasmashell\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-01-22 02:49+0100\n"
"PO-Revision-Date: 2014-08-30 22:41-0300\n"
"Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#: shellpackage.cpp:39 shellpackage.cpp:40
#, kde-format
msgid "Applets furniture"
msgstr "Disposição dos miniaplicativos"

#: shellpackage.cpp:41
#, kde-format
msgid "Explorer UI for adding widgets"
msgstr "Interface de exploração e adição de widgets"

#: shellpackage.cpp:42
#, kde-format
msgid "User interface for the views that will show containments"
msgstr "Interface de usuário das visualizações que mostrarão contêineres"

#: shellpackage.cpp:50
#, kde-format
msgid "Default layout file"
msgstr "Arquivo de layout padrão"

#: shellpackage.cpp:51
#, kde-format
msgid "Default plugins for containments, containmentActions, etc."
msgstr "Plugins predefinidos para contêineres, ações dos contêineres, etc."

#: shellpackage.cpp:56
#, kde-format
msgid "Error message shown when an applet fails to load"
msgstr ""
"Mensagem de erro apresentada quando um miniaplicativo não pode ser carregado"

#: shellpackage.cpp:57
#, kde-format
msgid "QML component that shows an applet in a popup"
msgstr "Componente QML que mostra um miniaplicativo em janela"

#: shellpackage.cpp:61
#, kde-format
msgid ""
"Compact representation of an applet when collapsed in a popup, for instance "
"as an icon. Applets can override this component."
msgstr ""
"Representação compacta de um miniaplicativo quando está fechado em uma "
"janela, por exemplo, como um ícone. Os miniaplicativos podem substituir este "
"componente."

#: shellpackage.cpp:66
#, kde-format
msgid "QML component for the configuration dialog for applets"
msgstr ""
"Componente QML para a caixa de diálogo de configuração dos miniaplicativos"

#: shellpackage.cpp:69
#, kde-format
msgid "QML component for the configuration dialog for containments"
msgstr "Componente QML para a caixa de diálogo de configuração dos contêineres"

#: shellpackage.cpp:70
#, kde-format
msgid "Panel configuration UI"
msgstr "Interface de configuração do painel"

#: shellpackage.cpp:73
#, kde-format
msgid "QML component for choosing an alternate applet"
msgstr "Componente QML para escolha de um miniaplicativo alternativo"

#: shellpackage.cpp:76
#, kde-format
msgid "Widgets explorer UI"
msgstr "Interface de exploração de widgets"

#: shellpackage.cpp:80
#, kde-format
msgid ""
"A UI for writing, loading and running desktop scripts in the current live "
"session"
msgstr ""
"Uma interface para gravar, carregar e executar scripts do ambiente de "
"trabalho na sessão atual"