# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vincent Pinon <vpinon@kde.org>, 2016.
# Simon Depiets <sdepiets@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-07-24 02:24+0200\n"
"PO-Revision-Date: 2019-07-21 16:06+0800\n"
"Last-Translator: Simon Depiets <sdepiets@gmail.com>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 19.07.70\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: HolidaysConfig.qml:57
#, kde-format
msgid "Search..."
msgstr "Chercher…"

#: HolidaysConfig.qml:104
#, kde-format
msgid "Region"
msgstr "Région"

#: HolidaysConfig.qml:108
#, kde-format
msgid "Name"
msgstr "Nom"

#: HolidaysConfig.qml:112
#, kde-format
msgid "Description"
msgstr "Description"