# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_engine_soliddevice\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-01-22 02:49+0100\n"
"PO-Revision-Date: 2020-07-15 03:55+0000\n"
"Last-Translator: transxx.py program <null@kde.org>\n"
"Language-Team: KDE Test Language <kde-i18n-doc@kde.org>\n"
"Language: x-test\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: soliddeviceengine.cpp:124
#, kde-format
msgid "Parent UDI"
msgstr "xxParent UDIxx"

#: soliddeviceengine.cpp:125
#, kde-format
msgid "Vendor"
msgstr "xxVendorxx"

#: soliddeviceengine.cpp:126
#, kde-format
msgid "Product"
msgstr "xxProductxx"

#: soliddeviceengine.cpp:127
#, kde-format
msgid "Description"
msgstr "xxDescriptionxx"

#: soliddeviceengine.cpp:128
#, kde-format
msgid "Icon"
msgstr "xxIconxx"

#: soliddeviceengine.cpp:129 soliddeviceengine.cpp:592
#, kde-format
msgid "Emblems"
msgstr "xxEmblemsxx"

#: soliddeviceengine.cpp:130 soliddeviceengine.cpp:476
#: soliddeviceengine.cpp:482 soliddeviceengine.cpp:495
#, kde-format
msgid "State"
msgstr "xxStatexx"

#: soliddeviceengine.cpp:131 soliddeviceengine.cpp:477
#: soliddeviceengine.cpp:483 soliddeviceengine.cpp:491
#: soliddeviceengine.cpp:493
#, kde-format
msgid "Operation result"
msgstr "xxOperation resultxx"

#: soliddeviceengine.cpp:132
#, kde-format
msgid "Timestamp"
msgstr "xxTimestampxx"

#: soliddeviceengine.cpp:140
#, kde-format
msgid "Processor"
msgstr "xxProcessorxx"

#: soliddeviceengine.cpp:141
#, kde-format
msgid "Number"
msgstr "xxNumberxx"

#: soliddeviceengine.cpp:142
#, kde-format
msgid "Max Speed"
msgstr "xxMax Speedxx"

#: soliddeviceengine.cpp:143
#, kde-format
msgid "Can Change Frequency"
msgstr "xxCan Change Frequencyxx"

#: soliddeviceengine.cpp:151
#, kde-format
msgid "Block"
msgstr "xxBlockxx"

#: soliddeviceengine.cpp:152
#, kde-format
msgid "Major"
msgstr "xxMajorxx"

#: soliddeviceengine.cpp:153
#, kde-format
msgid "Minor"
msgstr "xxMinorxx"

#: soliddeviceengine.cpp:154
#, kde-format
msgid "Device"
msgstr "xxDevicexx"

#: soliddeviceengine.cpp:162
#, kde-format
msgid "Storage Access"
msgstr "xxStorage Accessxx"

#: soliddeviceengine.cpp:163 soliddeviceengine.cpp:507
#: soliddeviceengine.cpp:606
#, kde-format
msgid "Accessible"
msgstr "xxAccessiblexx"

#: soliddeviceengine.cpp:164 soliddeviceengine.cpp:508
#, kde-format
msgid "File Path"
msgstr "xxFile Pathxx"

#: soliddeviceengine.cpp:179
#, kde-format
msgid "Storage Drive"
msgstr "xxStorage Drivexx"

#: soliddeviceengine.cpp:182
#, kde-format
msgid "Ide"
msgstr "xxIdexx"

#: soliddeviceengine.cpp:182
#, kde-format
msgid "Usb"
msgstr "xxUsbxx"

#: soliddeviceengine.cpp:182
#, kde-format
msgid "Ieee1394"
msgstr "xxIeee1394xx"

#: soliddeviceengine.cpp:182
#, kde-format
msgid "Scsi"
msgstr "xxScsixx"

#: soliddeviceengine.cpp:182
#, kde-format
msgid "Sata"
msgstr "xxSataxx"

#: soliddeviceengine.cpp:182
#, kde-format
msgid "Platform"
msgstr "xxPlatformxx"

#: soliddeviceengine.cpp:184
#, kde-format
msgid "Hard Disk"
msgstr "xxHard Diskxx"

#: soliddeviceengine.cpp:184
#, kde-format
msgid "Cdrom Drive"
msgstr "xxCdrom Drivexx"

#: soliddeviceengine.cpp:184
#, kde-format
msgid "Floppy"
msgstr "xxFloppyxx"

#: soliddeviceengine.cpp:184
#, kde-format
msgid "Tape"
msgstr "xxTapexx"

#: soliddeviceengine.cpp:184
#, kde-format
msgid "Compact Flash"
msgstr "xxCompact Flashxx"

#: soliddeviceengine.cpp:185
#, kde-format
msgid "Memory Stick"
msgstr "xxMemory Stickxx"

#: soliddeviceengine.cpp:185
#, kde-format
msgid "Smart Media"
msgstr "xxSmart Mediaxx"

#: soliddeviceengine.cpp:185
#, kde-format
msgid "SdMmc"
msgstr "xxSdMmcxx"

#: soliddeviceengine.cpp:185
#, kde-format
msgid "Xd"
msgstr "xxXdxx"

#: soliddeviceengine.cpp:187
#, kde-format
msgid "Bus"
msgstr "xxBusxx"

#: soliddeviceengine.cpp:188
#, kde-format
msgid "Drive Type"
msgstr "xxDrive Typexx"

#: soliddeviceengine.cpp:189 soliddeviceengine.cpp:202
#: soliddeviceengine.cpp:367 soliddeviceengine.cpp:381
#, kde-format
msgid "Removable"
msgstr "xxRemovablexx"

#: soliddeviceengine.cpp:190 soliddeviceengine.cpp:203
#: soliddeviceengine.cpp:368 soliddeviceengine.cpp:382
#, kde-format
msgid "Hotpluggable"
msgstr "xxHotpluggablexx"

#: soliddeviceengine.cpp:212
#, kde-format
msgid "Optical Drive"
msgstr "xxOptical Drivexx"

#: soliddeviceengine.cpp:217
#, kde-format
msgid "CD-R"
msgstr "xxCD-Rxx"

#: soliddeviceengine.cpp:220
#, kde-format
msgid "CD-RW"
msgstr "xxCD-RWxx"

#: soliddeviceengine.cpp:223
#, kde-format
msgid "DVD"
msgstr "xxDVDxx"

#: soliddeviceengine.cpp:226
#, kde-format
msgid "DVD-R"
msgstr "xxDVD-Rxx"

#: soliddeviceengine.cpp:229
#, kde-format
msgid "DVD-RW"
msgstr "xxDVD-RWxx"

#: soliddeviceengine.cpp:232
#, kde-format
msgid "DVD-RAM"
msgstr "xxDVD-RAMxx"

#: soliddeviceengine.cpp:235
#, kde-format
msgid "DVD+R"
msgstr "xxDVD+Rxx"

#: soliddeviceengine.cpp:238
#, kde-format
msgid "DVD+RW"
msgstr "xxDVD+RWxx"

#: soliddeviceengine.cpp:241
#, kde-format
msgid "DVD+DL"
msgstr "xxDVD+DLxx"

#: soliddeviceengine.cpp:244
#, kde-format
msgid "DVD+DLRW"
msgstr "xxDVD+DLRWxx"

#: soliddeviceengine.cpp:247
#, kde-format
msgid "BD"
msgstr "xxBDxx"

#: soliddeviceengine.cpp:250
#, kde-format
msgid "BD-R"
msgstr "xxBD-Rxx"

#: soliddeviceengine.cpp:253
#, kde-format
msgid "BD-RE"
msgstr "xxBD-RExx"

#: soliddeviceengine.cpp:256
#, kde-format
msgid "HDDVD"
msgstr "xxHDDVDxx"

#: soliddeviceengine.cpp:259
#, kde-format
msgid "HDDVD-R"
msgstr "xxHDDVD-Rxx"

#: soliddeviceengine.cpp:262
#, kde-format
msgid "HDDVD-RW"
msgstr "xxHDDVD-RWxx"

#: soliddeviceengine.cpp:264
#, kde-format
msgid "Supported Media"
msgstr "xxSupported Mediaxx"

#: soliddeviceengine.cpp:266
#, kde-format
msgid "Read Speed"
msgstr "xxRead Speedxx"

#: soliddeviceengine.cpp:267
#, kde-format
msgid "Write Speed"
msgstr "xxWrite Speedxx"

#: soliddeviceengine.cpp:275
#, kde-format
msgid "Write Speeds"
msgstr "xxWrite Speedsxx"

#: soliddeviceengine.cpp:283
#, kde-format
msgid "Storage Volume"
msgstr "xxStorage Volumexx"

#: soliddeviceengine.cpp:286
#, kde-format
msgid "Other"
msgstr "xxOtherxx"

#: soliddeviceengine.cpp:286
#, kde-format
msgid "Unused"
msgstr "xxUnusedxx"

#: soliddeviceengine.cpp:286
#, kde-format
msgid "File System"
msgstr "xxFile Systemxx"

#: soliddeviceengine.cpp:286
#, kde-format
msgid "Partition Table"
msgstr "xxPartition Tablexx"

#: soliddeviceengine.cpp:286
#, kde-format
msgid "Raid"
msgstr "xxRaidxx"

#: soliddeviceengine.cpp:286
#, kde-format
msgid "Encrypted"
msgstr "xxEncryptedxx"

#: soliddeviceengine.cpp:289 soliddeviceengine.cpp:291
#, kde-format
msgid "Usage"
msgstr "xxUsagexx"

#: soliddeviceengine.cpp:291
#, kde-format
msgid "Unknown"
msgstr "xxUnknownxx"

#: soliddeviceengine.cpp:294
#, kde-format
msgid "Ignored"
msgstr "xxIgnoredxx"

#: soliddeviceengine.cpp:295
#, kde-format
msgid "File System Type"
msgstr "xxFile System Typexx"

#: soliddeviceengine.cpp:296
#, kde-format
msgid "Label"
msgstr "xxLabelxx"

#: soliddeviceengine.cpp:297
#, kde-format
msgid "UUID"
msgstr "xxUUIDxx"

#: soliddeviceengine.cpp:306
#, kde-format
msgid "Encrypted Container"
msgstr "xxEncrypted Containerxx"

#: soliddeviceengine.cpp:318
#, kde-format
msgid "OpticalDisc"
msgstr "xxOpticalDiscxx"

#: soliddeviceengine.cpp:324
#, kde-format
msgid "Audio"
msgstr "xxAudioxx"

#: soliddeviceengine.cpp:327
#, kde-format
msgid "Data"
msgstr "xxDataxx"

#: soliddeviceengine.cpp:330
#, kde-format
msgid "Video CD"
msgstr "xxVideo CDxx"

#: soliddeviceengine.cpp:333
#, kde-format
msgid "Super Video CD"
msgstr "xxSuper Video CDxx"

#: soliddeviceengine.cpp:336
#, kde-format
msgid "Video DVD"
msgstr "xxVideo DVDxx"

#: soliddeviceengine.cpp:339
#, kde-format
msgid "Video Blu Ray"
msgstr "xxVideo Blu Rayxx"

#: soliddeviceengine.cpp:341
#, kde-format
msgid "Available Content"
msgstr "xxAvailable Contentxx"

#: soliddeviceengine.cpp:344
#, kde-format
msgid "Unknown Disc Type"
msgstr "xxUnknown Disc Typexx"

#: soliddeviceengine.cpp:344
#, kde-format
msgid "CD Rom"
msgstr "xxCD Romxx"

#: soliddeviceengine.cpp:344
#, kde-format
msgid "CD Recordable"
msgstr "xxCD Recordablexx"

#: soliddeviceengine.cpp:344
#, kde-format
msgid "CD Rewritable"
msgstr "xxCD Rewritablexx"

#: soliddeviceengine.cpp:344
#, kde-format
msgid "DVD Rom"
msgstr "xxDVD Romxx"

#: soliddeviceengine.cpp:345
#, kde-format
msgid "DVD Ram"
msgstr "xxDVD Ramxx"

#: soliddeviceengine.cpp:345
#, kde-format
msgid "DVD Recordable"
msgstr "xxDVD Recordablexx"

#: soliddeviceengine.cpp:345
#, kde-format
msgid "DVD Rewritable"
msgstr "xxDVD Rewritablexx"

#: soliddeviceengine.cpp:345
#, kde-format
msgid "DVD Plus Recordable"
msgstr "xxDVD Plus Recordablexx"

#: soliddeviceengine.cpp:346
#, kde-format
msgid "DVD Plus Rewritable"
msgstr "xxDVD Plus Rewritablexx"

#: soliddeviceengine.cpp:346
#, kde-format
msgid "DVD Plus Recordable Duallayer"
msgstr "xxDVD Plus Recordable Duallayerxx"

#: soliddeviceengine.cpp:346
#, kde-format
msgid "DVD Plus Rewritable Duallayer"
msgstr "xxDVD Plus Rewritable Duallayerxx"

#: soliddeviceengine.cpp:347
#, kde-format
msgid "Blu Ray Rom"
msgstr "xxBlu Ray Romxx"

#: soliddeviceengine.cpp:347
#, kde-format
msgid "Blu Ray Recordable"
msgstr "xxBlu Ray Recordablexx"

#: soliddeviceengine.cpp:347
#, kde-format
msgid "Blu Ray Rewritable"
msgstr "xxBlu Ray Rewritablexx"

#: soliddeviceengine.cpp:347
#, kde-format
msgid "HD DVD Rom"
msgstr "xxHD DVD Romxx"

#: soliddeviceengine.cpp:348
#, kde-format
msgid "HD DVD Recordable"
msgstr "xxHD DVD Recordablexx"

#: soliddeviceengine.cpp:348
#, kde-format
msgid "HD DVD Rewritable"
msgstr "xxHD DVD Rewritablexx"

#: soliddeviceengine.cpp:350
#, kde-format
msgid "Disc Type"
msgstr "xxDisc Typexx"

#: soliddeviceengine.cpp:351
#, kde-format
msgid "Appendable"
msgstr "xxAppendablexx"

#: soliddeviceengine.cpp:352
#, kde-format
msgid "Blank"
msgstr "xxBlankxx"

#: soliddeviceengine.cpp:353
#, kde-format
msgid "Rewritable"
msgstr "xxRewritablexx"

#: soliddeviceengine.cpp:354
#, kde-format
msgid "Capacity"
msgstr "xxCapacityxx"

#: soliddeviceengine.cpp:362
#, kde-format
msgid "Camera"
msgstr "xxCameraxx"

#: soliddeviceengine.cpp:364 soliddeviceengine.cpp:378
#, kde-format
msgid "Supported Protocols"
msgstr "xxSupported Protocolsxx"

#: soliddeviceengine.cpp:365 soliddeviceengine.cpp:379
#, kde-format
msgid "Supported Drivers"
msgstr "xxSupported Driversxx"

#: soliddeviceengine.cpp:376
#, kde-format
msgid "Portable Media Player"
msgstr "xxPortable Media Playerxx"

#: soliddeviceengine.cpp:390
#, kde-format
msgid "Battery"
msgstr "xxBatteryxx"

#: soliddeviceengine.cpp:393
#, kde-format
msgid "Unknown Battery"
msgstr "xxUnknown Batteryxx"

#: soliddeviceengine.cpp:393
#, kde-format
msgid "PDA Battery"
msgstr "xxPDA Batteryxx"

#: soliddeviceengine.cpp:393
#, kde-format
msgid "UPS Battery"
msgstr "xxUPS Batteryxx"

#: soliddeviceengine.cpp:393
#, kde-format
msgid "Primary Battery"
msgstr "xxPrimary Batteryxx"

#: soliddeviceengine.cpp:394
#, kde-format
msgid "Mouse Battery"
msgstr "xxMouse Batteryxx"

#: soliddeviceengine.cpp:394
#, kde-format
msgid "Keyboard Battery"
msgstr "xxKeyboard Batteryxx"

#: soliddeviceengine.cpp:394
#, kde-format
msgid "Keyboard Mouse Battery"
msgstr "xxKeyboard Mouse Batteryxx"

#: soliddeviceengine.cpp:394
#, kde-format
msgid "Camera Battery"
msgstr "xxCamera Batteryxx"

#: soliddeviceengine.cpp:395
#, kde-format
msgid "Phone Battery"
msgstr "xxPhone Batteryxx"

#: soliddeviceengine.cpp:395
#, kde-format
msgid "Monitor Battery"
msgstr "xxMonitor Batteryxx"

#: soliddeviceengine.cpp:395
#, kde-format
msgid "Gaming Input Battery"
msgstr "xxGaming Input Batteryxx"

#: soliddeviceengine.cpp:395
#, kde-format
msgid "Bluetooth Battery"
msgstr "xxBluetooth Batteryxx"

#: soliddeviceengine.cpp:398
#, kde-format
msgid "Not Charging"
msgstr "xxNot Chargingxx"

#: soliddeviceengine.cpp:398
#, kde-format
msgid "Charging"
msgstr "xxChargingxx"

#: soliddeviceengine.cpp:398
#, kde-format
msgid "Discharging"
msgstr "xxDischargingxx"

#: soliddeviceengine.cpp:398
#, kde-format
msgid "Fully Charged"
msgstr "xxFully Chargedxx"

#: soliddeviceengine.cpp:400
#, kde-format
msgid "Plugged In"
msgstr "xxPlugged Inxx"

#: soliddeviceengine.cpp:401
#, kde-format
msgid "Type"
msgstr "xxTypexx"

#: soliddeviceengine.cpp:402
#, kde-format
msgid "Charge Percent"
msgstr "xxCharge Percentxx"

#: soliddeviceengine.cpp:403
#, kde-format
msgid "Rechargeable"
msgstr "xxRechargeablexx"

#: soliddeviceengine.cpp:404
#, kde-format
msgid "Charge State"
msgstr "xxCharge Statexx"

#: soliddeviceengine.cpp:427
#, kde-format
msgid "Type Description"
msgstr "xxType Descriptionxx"

#: soliddeviceengine.cpp:432
#, kde-format
msgid "Device Types"
msgstr "xxDevice Typesxx"

#: soliddeviceengine.cpp:456 soliddeviceengine.cpp:553
#, kde-format
msgid "Size"
msgstr "xxSizexx"

#: soliddeviceengine.cpp:535
#, kde-format
msgid "Filesystem is not responding"
msgstr "xxFilesystem is not respondingxx"

#: soliddeviceengine.cpp:535
#, kde-format
msgid "Filesystem mounted at '%1' is not responding"
msgstr "xxFilesystem mounted at '%1' is not respondingxx"

#: soliddeviceengine.cpp:551
#, kde-format
msgid "Free Space"
msgstr "xxFree Spacexx"

#: soliddeviceengine.cpp:552
#, kde-format
msgid "Free Space Text"
msgstr "xxFree Space Textxx"

#: soliddeviceengine.cpp:554
#, kde-format
msgid "Size Text"
msgstr "xxSize Textxx"

#: soliddeviceengine.cpp:580
#, kde-format
msgid "Temperature"
msgstr "xxTemperaturexx"

#: soliddeviceengine.cpp:581
#, kde-format
msgid "Temperature Unit"
msgstr "xxTemperature Unitxx"

#: soliddeviceengine.cpp:625 soliddeviceengine.cpp:629
#, kde-format
msgid "In Use"
msgstr "xxIn Usexx"